import Foundation

class NetworkManager: ObservableObject {
    @Published var guessedAge: GuessedAge?
    
    func fetchData(name: String) {
        guard let url = URL(string: "https://api.agify.io/?name=\(name)") else { return }
        URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            guard let safeData = data else { return }
            let decoder = JSONDecoder()
            do {
                let result = try decoder.decode(GuessedAge.self, from: safeData)
                DispatchQueue.main.async {
                    self.guessedAge = result
                }
            } catch {
                print(error)
            }
        }.resume()
    }
}

struct GuessedAge: Decodable {
    let name: String
    let age: Int
}
