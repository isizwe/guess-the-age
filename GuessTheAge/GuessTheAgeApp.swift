import SwiftUI

@main
struct GuessTheAgeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
