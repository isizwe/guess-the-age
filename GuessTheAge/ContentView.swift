import SwiftUI

struct ContentView: View {
    @ObservedObject var networkManager = NetworkManager()
    @State var name: String = ""
    
    var body: some View {
        VStack {
            Text(String(networkManager.guessedAge?.age ?? 0))
                .font(.system(size: 50))
            Divider()
            
            TextField("Enter a name to guess age", text: $name)
                .frame(height: 50)
                .padding(.horizontal)
                .border(Color.black, width: 2)
            
            Button(action: {
                networkManager.fetchData(name: name)
            }) {
                Text("Guess the age")
                    .foregroundColor(.white)
                    .font(.title3)
                    .fontWeight(.heavy)
            }
            .padding()
            .background(Color.blue)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
